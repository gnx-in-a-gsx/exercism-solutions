import re
import string


def word_count(phrase):
    phrase = re.sub('_', ' ', phrase.lower().strip(string.punctuation))
    print(phrase)
    phrase_split = list(re.findall(r'(\w+(?:\'\w)|\w+)', phrase))
    print(phrase_split)
    return_dict = dict()
    [return_dict.update([(phrase_split[i], return_dict.get(phrase_split[i], 0) + 1)]) for i in
     range(len(phrase_split))]
    return return_dict
