class Matrix(object):
    def __init__(self, matrix_string):
        self.matrix = [list(map(int, row.split(' '))) for row in matrix_string.split('\n')]
        self.columns = [list() for col in self.matrix[0]]
        [self.columns[i].append(row[i]) for row in self.matrix for i in range(len(row))]

    def row(self, index):
        return self.matrix[index - 1]

    def column(self, index):
        return self.columns[index - 1]
