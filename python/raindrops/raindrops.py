def raindrops(number):
    multiples = dict([(3, 'Pling'), (5, 'Plang'), (7, 'Plong')])
    string = ''.join([value for key, value in multiples.items() if number % key == 0])
    return string if string != '' else str(number)
