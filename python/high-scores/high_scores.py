class HighScores(object):
    def __init__(self, scores):
        self.scores = scores
        self.total_scores = len(scores)
        self.top_three = sorted(scores, reverse=True)[0:3]

    def latest(self):
        return self.scores[self.total_scores - 1]

    def personal_best(self):
        return self.top_three[0]

    def personal_top_three(self):
        return self.top_three
