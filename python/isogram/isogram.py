import re


def is_isogram(string):
    pattern = re.compile('[\W]')
    string = string.lower().replace(' ', '')
    subbed = pattern.sub('', string)
    return len({subbed[i] for i in range(len(subbed))}) == len(subbed)
