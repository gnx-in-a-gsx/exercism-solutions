number_to_string = {
    1: 'first',
    2: 'second',
    3: 'third',
    4: 'fourth',
    5: 'fifth',
    6: 'sixth',
    7: 'seventh',
    8: 'eighth',
    9: 'ninth',
    10: 'tenth',
    11: 'eleventh',
    12: 'twelfth'
}
string_lookup = [
    'a Partridge in a Pear Tree.',
    'two Turtle Doves',
    'three French Hens',
    'four Calling Birds',
    'five Gold Rings',
    'six Geese-a-Laying',
    'seven Swans-a-Swimming',
    'eight Maids-a-Milking',
    'nine Ladies Dancing',
    'ten Lords-a-Leaping',
    'eleven Pipers Piping',
    'twelve Drummers Drumming'
]


def recite(start_verse, end_verse):
    return ["On the " + number_to_string.get(i) + " day of Christmas my true love gave to me: " + ', '.join(
        [string_lookup[k - 1] for k in range(i, 1, -1)]) + (', and ' if i > 1 else '') + string_lookup[0] for i in
            range(start_verse, end_verse + 1)]
