import re


class Luhn(object):
    def __init__(self, card_num):
        self.card_num = 'INVALID' if len(re.findall('[^\d\s]', card_num)) > 0 else list(
            map(int, list(card_num.strip(' ').replace(' ', ''))))

    def is_valid(self):
        return False if self.card_num == 'INVALID' or len(self.card_num) == 1 else sum(
            [self.card_num[i] if ((i + 1 if len(self.card_num) % 2 == 1 else i) % 2) != 0 else (
                self.card_num[i] * 2 if self.card_num[i] * 2 < 10 else self.card_num[i] * 2 - 9
            ) for i in range(len(self.card_num))]) % 10 == 0
